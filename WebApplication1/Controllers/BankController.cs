﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Models.HelperModels;

namespace WebApplication1.Controllers
{
    public class BankController : Controller
    {

        private readonly BankRepository bankRepository = new BankRepository();
        
        //
        // GET: /Bank/
        public ActionResult Index()
        {
            var banks = bankRepository.GetAllBanks();
            
            if(banks == null)
            {
                ViewBag.Message = "Error retreiving banks from service.";
                return View("Error");
            }

            return View(banks);
        }

        // GET: /Bank/Details/Guid
        public ActionResult Details(string id)
        {
            if(string.IsNullOrWhiteSpace(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var bankGuid = new Guid(id);
            Bank bank = bankRepository.GetBankById(bankGuid);

            if(bank == null)
            {
                ViewBag.Message = "Error retreiving data for bank number: " + id + ".";
                return View("Error");
            }

            List<BankType> bankTypesList = new List<BankType>();
            bankTypesList.Add(new BankType(1, "Dobra banka"));
            bankTypesList.Add(new BankType(2, "Mnogo dobra banka"));
            bankTypesList.Add(new BankType(3, "Najbolja banka"));
            bankTypesList.Add(new BankType(4, "Loša banka"));

            List<BankStatus> bankStatusesList = bankRepository.GetBankStatuses();
            // ViewBag.bankStatuses = new SelectList(bankStatusesList, "BankStatusID", "Name");
            ViewData["bankStatuses"] = bankStatusesList;
            ViewBag.bankTypes = bankTypesList;

            return View(bank);
        }
	}
}