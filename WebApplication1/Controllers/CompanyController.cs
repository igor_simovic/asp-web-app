﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CompanyController : Controller
    {
        private readonly CompanyRepository companyRepository = new CompanyRepository();

        //
        // GET: /Company/
        public ActionResult Index(string searchString, int? companyTypes)
        {
            //Zato što šugavi servis vraća najviše 100 rezultata
            if (string.IsNullOrWhiteSpace(searchString))
                searchString = "CUB";

            List<CompanyType> companyTypesList = companyRepository.GetCompanyTypes();
            ViewBag.companyTypes = new SelectList(companyTypesList, "CompanyTypeID", "Name");

            var companies = companyRepository.GetAllCompanies(searchString, companyTypes);
            if (companies == null)
            {
                ViewBag.Message = "Error retreiving companies from service.";
                return View("Error");
            }

            return View(companies);
        }

        // GET: /Company/Details/Guid
        public ActionResult Details(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var companyGuid = new Guid(id);
            Company company = companyRepository.GetCompanyById(companyGuid);

            if (company == null)
            {
                ViewBag.Message = "Error retreiving data for company number: " + id + ".";
                return View("Error");
            }
            return View(company);
        }
	}
}