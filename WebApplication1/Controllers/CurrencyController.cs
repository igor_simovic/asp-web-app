﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models.Repositories;

namespace WebApplication1.Controllers
{
    public class CurrencyController : Controller
    {
        private readonly CurrencyRepository currenyRepository = new CurrencyRepository();
        
        //
        // GET: /Currency/
        public ActionResult Index()
        {
            var currencies = currenyRepository.GetAllCurrencies();

            return View();
        }
	}
}