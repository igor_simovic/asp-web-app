﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;


namespace WebApplication1.Models
{

    using System.Xml.Serialization;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CurrencyDataSet
    {

        private Currency[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Currency", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Currency[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class Currency
    {

        private string currencyIDField;

        private int currencyCodeField;

        private string currencyCodeNumCharField;

        private string currencyCodeAlfaCharField;

        private int unitField;

        private short convertibleField;

        private bool convertibleFieldSpecified;

        private string currencyNameSerCyrlField;

        private string currencyNameSerLatField;

        private string currencyNameEngField;

        private int indicatorField;

        private bool indicatorFieldSpecified;

        private string countryIDField;

        private int countryCodeField;

        private bool countryCodeFieldSpecified;

        private string countryCodeNumCharField;

        private string countryCodeAlfaChar3Field;

        private string countryCodeAlfaChar2Field;

        private string countryNameSerCyrlField;

        private string countryNameSerLatField;

        private string countryNameEngField;


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CurrencyID
        {
            get
            {
                return this.currencyIDField;
            }
            set
            {
                this.currencyIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int CurrencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CurrencyCodeNumChar
        {
            get
            {
                return this.currencyCodeNumCharField;
            }
            set
            {
                this.currencyCodeNumCharField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CurrencyCodeAlfaChar
        {
            get
            {
                return this.currencyCodeAlfaCharField;
            }
            set
            {
                this.currencyCodeAlfaCharField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int Unit
        {
            get
            {
                return this.unitField;
            }
            set
            {
                this.unitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public short Convertible
        {
            get
            {
                return this.convertibleField;
            }
            set
            {
                this.convertibleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ConvertibleSpecified
        {
            get
            {
                return this.convertibleFieldSpecified;
            }
            set
            {
                this.convertibleFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CurrencyNameSerCyrl
        {
            get
            {
                return this.currencyNameSerCyrlField;
            }
            set
            {
                this.currencyNameSerCyrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CurrencyNameSerLat
        {
            get
            {
                return this.currencyNameSerLatField;
            }
            set
            {
                this.currencyNameSerLatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CurrencyNameEng
        {
            get
            {
                return this.currencyNameEngField;
            }
            set
            {
                this.currencyNameEngField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int Indicator
        {
            get
            {
                return this.indicatorField;
            }
            set
            {
                this.indicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IndicatorSpecified
        {
            get
            {
                return this.indicatorFieldSpecified;
            }
            set
            {
                this.indicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CountryID
        {
            get
            {
                return this.countryIDField;
            }
            set
            {
                this.countryIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CountryCodeSpecified
        {
            get
            {
                return this.countryCodeFieldSpecified;
            }
            set
            {
                this.countryCodeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CountryCodeNumChar
        {
            get
            {
                return this.countryCodeNumCharField;
            }
            set
            {
                this.countryCodeNumCharField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CountryCodeAlfaChar3
        {
            get
            {
                return this.countryCodeAlfaChar3Field;
            }
            set
            {
                this.countryCodeAlfaChar3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CountryCodeAlfaChar2
        {
            get
            {
                return this.countryCodeAlfaChar2Field;
            }
            set
            {
                this.countryCodeAlfaChar2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CountryNameSerCyrl
        {
            get
            {
                return this.countryNameSerCyrlField;
            }
            set
            {
                this.countryNameSerCyrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CountryNameSerLat
        {
            get
            {
                return this.countryNameSerLatField;
            }
            set
            {
                this.countryNameSerLatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CountryNameEng
        {
            get
            {
                return this.countryNameEngField;
            }
            set
            {
                this.countryNameEngField = value;
            }
        }
    }
}