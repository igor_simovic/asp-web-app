﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.HelperModels
{
    public class BankType
    {
        public BankType(int bankTypeId, string name)
        {
            this.BankTypeID = bankTypeId;
            this.Name = name;
        }
        public int BankTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}