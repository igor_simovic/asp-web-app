﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplication1.CoreXmlService;
using WebApplication1.Models.HelperModels;
using WebApplication1.Models.Repositories.XmlHelper;

namespace WebApplication1.Models
{
    public class BankRepository
    {
        private AuthenticationHeader ah;
        private CoreXmlService.CoreXmlServiceSoapClient coreService;

        public BankRepository()
        {
            this.coreService = new CoreXmlServiceSoapClient();
            this.ah = new AuthenticationHeader();
            this.ah.UserName = "agentplus";
            this.ah.Password = "agppass*1";
            this.ah.LicenceID = new Guid("eb1eb597-6846-4f4e-8f5e-4b300c29fc1b");
        }

        public List<Bank> GetAllBanks()
        {
            string xmlResult;
            List<Bank> result;

            try
            {
                xmlResult = this.coreService.GetBank(this.ah, null, null, null, string.Empty, string.Empty);
                result = BankXmlHelper.GetBanksFromXml(xmlResult);
            }
            catch(Exception)
            {
                return null;
            }

            return result;
        }

        public Bank GetBankById(Guid id)
        {
            string xmlResult;
            Bank result;
            
            try
            {
                xmlResult = this.coreService.GetBank(this.ah, id, null, null, string.Empty, string.Empty);
                result = BankXmlHelper.GetBanksFromXml(xmlResult)[0];
            }
            catch(Exception)
            {
                return null;
            }
            
            return result;
        }

        internal List<BankStatus> GetBankStatuses()
        {
            string xmlResult;
            List<BankStatus> result;

            try
            {
                xmlResult = this.coreService.GetBankStatus(this.ah, null);
                result = BankXmlHelper.GetBankStatusesFromXml(xmlResult);
            }
            catch (Exception)
            {
                return null;
            }

            return result;
        }


    }
}