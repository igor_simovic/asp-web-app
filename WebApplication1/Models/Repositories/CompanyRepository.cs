﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using WebApplication1.CoreXmlService;
using WebApplication1.Models.HelperModels;
using WebApplication1.Models.Repositories.XmlHelper;

namespace WebApplication1.Models
{
    public class CompanyRepository
    {
        private AuthenticationHeader ah;
        private CoreXmlService.CoreXmlServiceSoapClient coreService;

        public CompanyRepository()
        {
            this.coreService = new CoreXmlServiceSoapClient();
            this.ah = new AuthenticationHeader();
            this.ah.UserName = "agentplus";
            this.ah.Password = "agppass*1";
            this.ah.LicenceID = new Guid("eb1eb597-6846-4f4e-8f5e-4b300c29fc1b");
        }

        public List<Company> GetAllCompanies(string name, int? companyTypeID)
        {
            string xmlResult;
            List<Company> result;

            try
            {
                xmlResult = this.coreService.GetCompany(this.ah, null, null, name, string.Empty, null, null, null, null);
                result = CompanyXmlHelper.GetCompaniesFromXml(xmlResult);
            }
            catch(Exception)
            {
                return null;
            }

            return result;
        }

        public Company GetCompanyById(Guid id)
        {
            string xmlResult;
            Company result;
            try
            {
                xmlResult = this.coreService.GetCompany(this.ah, id, null, string.Empty, string.Empty, null, null, null, null);
                result = CompanyXmlHelper.GetCompaniesFromXml(xmlResult)[0];
            }
            catch(Exception)
            {
                return null;
            }

            return result;
        }

        internal List<CompanyType> GetCompanyTypes()
        {
            string xmlResult;
            List<CompanyType> result;

            try
            {
                xmlResult = this.coreService.GetCompanyType(this.ah, null);
                result = CompanyXmlHelper.GetCompanyTypesFromXml(xmlResult);
            }
            catch (Exception)
            {
                return null;
            }

            return result;
        }

        internal List<CompanyStatus> GetCompanyStatuses()
        {
            string xmlResult;
            List<CompanyStatus> result;

            try
            {
                xmlResult = this.coreService.GetCompanyStatus(this.ah, null);
                result = CompanyXmlHelper.GetCompanyStatusesFromXml(xmlResult);
            }
            catch (Exception)
            {
                return null;
            }

            return result;
        }
    }
}