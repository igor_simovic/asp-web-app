﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using WebApplication1.Models.HelperModels;

namespace WebApplication1.Models.Repositories.XmlHelper
{
    public static class BankXmlHelper
    {
        public static List<Bank> GetBanksFromXml(string xmlResult)
        {
            BankDataSet banksList;
            List<Bank> result = new List<Bank>();

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "BankDataSet";
            xRoot.IsNullable = true;

            XmlSerializer ser = new XmlSerializer(typeof(BankDataSet), xRoot);

            using (XmlTextReader r = new XmlTextReader(new StringReader(xmlResult)))
            {
                banksList = (BankDataSet)ser.Deserialize(r);
            }

            return banksList.Items.ToList();
        }

        public static List<BankStatus> GetBankStatusesFromXml(string xmlResult)
        {
            BankStatusDataSet BankStatussList;
            List<BankStatus> result = new List<BankStatus>();

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "BankDataSet";
            xRoot.IsNullable = true;

            XmlSerializer ser = new XmlSerializer(typeof(BankStatusDataSet), xRoot);

            using (XmlTextReader r = new XmlTextReader(new StringReader(xmlResult)))
            {
                BankStatussList = (BankStatusDataSet)ser.Deserialize(r);
            }

            return BankStatussList.Items.ToList();
        }
        
    }
}