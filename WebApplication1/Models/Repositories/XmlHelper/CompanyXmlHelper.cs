﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using WebApplication1.Models.HelperModels;

namespace WebApplication1.Models.Repositories.XmlHelper
{
    public static class CompanyXmlHelper
    {
        public static List<Company> GetCompaniesFromXml(string xmlResult)
        {
            CompanyDataSet companiesList;
            List<Company> result = new List<Company>();

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "CompanyDataSet";
            xRoot.IsNullable = true;

            XmlSerializer ser = new XmlSerializer(typeof(CompanyDataSet), xRoot);

            using (XmlTextReader r = new XmlTextReader(new StringReader(xmlResult)))
            {
                companiesList = (CompanyDataSet)ser.Deserialize(r);
            }

            return companiesList.Items.ToList();
        }


        public static List<CompanyType> GetCompanyTypesFromXml(string xmlResult)
        {
            CompanyTypeDataSet companyTypesList;
            List<CompanyType> result = new List<CompanyType>();

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "CompanyTypeDataSet";
            xRoot.IsNullable = true;

            XmlSerializer ser = new XmlSerializer(typeof(CompanyTypeDataSet), xRoot);

            using (XmlTextReader r = new XmlTextReader(new StringReader(xmlResult)))
            {
                companyTypesList = (CompanyTypeDataSet)ser.Deserialize(r);
            }

            return companyTypesList.Items.ToList();
        }

        public static List<CompanyStatus> GetCompanyStatusesFromXml(string xmlResult)
        {
            CompanyStatusDataSet CompanyStatusesList;
            List<CompanyStatus> result = new List<CompanyStatus>();

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "CompanyStatusDataSet";
            xRoot.IsNullable = true;

            XmlSerializer ser = new XmlSerializer(typeof(CompanyStatusDataSet), xRoot);

            using (XmlTextReader r = new XmlTextReader(new StringReader(xmlResult)))
            {
                CompanyStatusesList = (CompanyStatusDataSet)ser.Deserialize(r);
            }

            return CompanyStatusesList.Items.ToList();
        }
    }
}